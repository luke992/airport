import org.junit.Test;
import static org.junit.Assert.*;

public class TestsCheckInDesk {

    @Test
    public void checkInAPassengerTheyGetABoardingPass() {
        Airport airport = new Airport("LHR");
        Plane plane = new Plane("TE55no");
        airport.landPlane(plane);
        Passenger passenger = new Passenger("Dave");
        passenger.setFlightNo("TE55no");
        assertNull(passenger.getBoardingPass());
        airport.checkInDesks[0].checkIn(passenger);
        assertNotNull(passenger.getBoardingPass());
    }
    @Test
    public void checkInAPassengerTheirBagsAreTaken() {
        Airport airport = new Airport("LHR");
        Plane plane = new Plane("TE55no");
        airport.landPlane(plane);
        Bag bag = new Bag(10);
        Passenger passenger = new Passenger("Dave");
        passenger.setFlightNo("TE55no");
        passenger.setBag(bag);
        airport.checkInDesks[0].checkIn(passenger);
        assertNull(passenger.getBag());
    }
    @Test
    public void shouldHaveAccessToPlanes() {
        Airport airport = new Airport("LHR");
        Plane plane = new Plane("TE55no");
        airport.landPlane(plane);
        Passenger passenger = new Passenger("Greg");
        passenger.setFlightNo("TE55no");
        airport.checkInDesks[0].checkIn(passenger);
        assertFalse(plane.isSeatEmpty(passenger.getBoardingPass()));
    }
}


