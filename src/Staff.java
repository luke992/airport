public class Staff {
    String position;
    private Airport airportName;
    private int job;

    public Staff(String position) {
        this.position = position;
    }

    public String getName() {
        return position;
    }

    public void setAirport(Airport airportName) {
        this.airportName = airportName;
    }

    public Airport getairport() {
        return airportName;
    }

    public int[] getPosition() {
        return new int[] { job };
    }

}
