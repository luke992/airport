import org.junit.Test;
import static org.junit.Assert.*;

public class TestsPassenger {

    @Test
    public void APassengerHasAName() {
        Passenger passenger = new Passenger("david");
        assertEquals(passenger.getName(), "david");
    }

    @Test
    public void aPassengerHasAAirport() {
        Airport Gatwick = new Airport("Gatwick");
        Passenger passenger = new Passenger("david");
        assertNull(passenger.getAirport());
        passenger.setAirport(Gatwick);
        assertSame(passenger.getAirport(), Gatwick);
    }

    @Test
    public void APassengerCanHaveABag() {
        Passenger passenger = new Passenger("Bob");
        Bag bag = new Bag(17);
        passenger.setBag(bag);
        assertSame(passenger.getBag(), bag);
    }

    @Test
    public void APassengerChecksInBag() {
        Passenger passenger = new Passenger("Bob");
        Bag bag = new Bag(17);
        passenger.setBag(bag);
        passenger.getBag();
        assertSame(passenger.getBag(), null);
    }

    @Test
    public void aPassengerCanHaveABoardingPass() {
        Passenger passenger = new Passenger("Bob");
        BoardingPass pass = new BoardingPass(1, 0);
        assertNull(passenger.getBoardingPass());
        passenger.setBoardingPass(pass);
        assertSame(passenger.getBoardingPass(), pass);

    }

}
