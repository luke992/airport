import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class TestsAirport {
    @Test
    public void AirportHasAName() {
        Airport gatwick = new Airport("Gatwick");
        assertEquals(gatwick.getAirportName(), "Gatwick");
        System.out.println("Welcome to " + gatwick.getAirportName() + " Airport");
    }

    @Test
    public void aPassengerCanHaveABoardingPass() {
        Passenger passenger = new Passenger("Bob");
        BoardingPass pass = new BoardingPass(1, 0);
        assertNull(passenger.getBoardingPass());
        passenger.setBoardingPass(pass);
        assertSame(passenger.getBoardingPass(), pass);
    }

    @Test
    public void checkInPassenger() {
        Airport Gatwick = new Airport("Gatwick");
        Passenger passenger = new Passenger("david");
        Bag bag = new Bag(17);
        passenger.setBag(bag);
        assertSame(passenger.getBag(), bag);
        assertNull(passenger.getAirport());
        passenger.setAirport(Gatwick);
        assertSame(passenger.getAirport(), Gatwick);
        BoardingPass pass = new BoardingPass(1, 0);
        assertNull(passenger.getBoardingPass());
        passenger.setBoardingPass(pass);
        assertSame(passenger.getBoardingPass(), pass);
    }
    @Test
    public void parkPlanesAtAnAirport() {
        Plane plane = new Plane("LA993");
        Airport london = new Airport("LHR");
        london.landPlane(plane);
        assertEquals(london.planeAt("LA993"), plane);
    }
    @Test
    public void hasCheckinDesks() {
        Airport belfast = new Airport("BEL");
        assertTrue(belfast.checkInDesks[0] instanceof CheckInDesk);
    }
}

