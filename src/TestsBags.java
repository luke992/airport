import static org.junit.Assert.*;

import org.junit.Test;

public class TestsBags {
    @Test
    public void test_weight() {
        Bag bag = new Bag(13);
        assertEquals(bag.getWeight(), 13);
    }

    @Test
    public void test_weight_errors() {
        Bag bag = new Bag(12);
        assertTrue(bag instanceof Bag);
    }

    /*
     * @Test
     * public String testMaxWeight() {
     * Bag bag = new Bag(12);
     * int weight = bag.getPassengerBag();
     * if(bag.getWeight() < 20) {
     * return DropBag();
     * }else {
     * return "Weight exceeded please lower bag weight";
     * }
     * }
     */

}
