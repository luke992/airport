public class Passenger {
    private String name;
    private BoardingPass boardingPass;
    private Bag bags;
    private Airport airportName;
    private String flightNo;

    public Passenger(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setBoardingPass(BoardingPass pass) {
        this.boardingPass = pass;
    }

    public BoardingPass getBoardingPass() {
        return boardingPass;
    }

    public void setBag(Bag bag) {
        this.bags = bag;
    }

    public Bag getBag() {
        Bag bag = this.bags;
        this.bags = null;
        return bag;
    }

    public void setAirport(Airport airportName) {
        this.airportName = airportName;
    }

    public Airport getAirport() {
        return airportName;
    }

    public void setFlightNo(String flightNo) {
        this.flightNo = flightNo;
    }

    public String getFlightNo() {
        return this.flightNo;
    }

}