import org.junit.Test;
import static org.junit.Assert.*;

public class TestsPlane {

    @Test
    public void staffAssignedToPlane() {
        Plane Tui = new Plane("Tui");
        Staff pilot = new Staff("pilot");
        Tui.assignStaff(pilot);

    }

    @Test
    public void aPassengerCanBoardPlane() {
        Airport Gatwick = new Airport("Gatwick");
        Passenger passenger = new Passenger("david");
        assertNull(passenger.getAirport());
        passenger.setAirport(Gatwick);
        assertSame(passenger.getAirport(), Gatwick);
        BoardingPass pass = new BoardingPass(1, 0);
        assertNull(passenger.getBoardingPass());
        passenger.setBoardingPass(pass);
        assertSame(passenger.getBoardingPass(), pass);
        Plane Tui = new Plane("Tui");
        assertEquals(Tui.isSeatEmpty(pass), true);
        Tui.boardPassenger(passenger);
        assertEquals(Tui.isSeatEmpty(pass), false);
    }

    @Test
    public void aBagAllocatedToThePlaneHullAndPassengerToSeatAndStaff() {
        Airport Gatwick = new Airport("Gatwick");
        Passenger passenger = new Passenger("david");
        assertNull(passenger.getAirport());
        passenger.setAirport(Gatwick);
        assertSame(passenger.getAirport(), Gatwick);
        Bag bag = new Bag(17);
        passenger.setBag(bag);
        assertSame(passenger.getBag(), bag);
        BoardingPass pass = new BoardingPass(1, 0);
        assertNull(bag.getBoardingPass());
        bag.setBoardingPass(pass);
        assertSame(bag.getBoardingPass(), pass);
        assertNull(passenger.getBoardingPass());
        passenger.setBoardingPass(pass);
        assertSame(passenger.getBoardingPass(), pass);
        Plane Tui = new Plane("Tui");
        Staff pilot = new Staff("pilot");
        Tui.assignStaff(pilot);
        assertEquals(Tui.isHullEmpty(pass), true);
        Tui.PutBagInHull(bag);
        assertEquals(Tui.isHullEmpty(pass), false);
        assertEquals(Tui.isSeatEmpty(pass), true);
        Tui.boardPassenger(passenger);
        assertEquals(Tui.isSeatEmpty(pass), false);
    }

}
