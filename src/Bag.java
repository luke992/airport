public class Bag {

    private int weight;
    private BoardingPass boardingPass;

    public Bag(int weight) {
        this.weight = weight;
    }

    public int getWeight() {
        return this.weight;
    }

    public void setBoardingPass(BoardingPass pass) {
        this.boardingPass = pass;
    }

    public BoardingPass getBoardingPass() {
        return boardingPass;
    }

}
