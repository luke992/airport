public class Plane {

    // ArrayList<Staff> position = new ArrayList<Staff>();
    // Staff pilot = new Staff("pilot");
    // Staff coPilot = new Staff("co-pilot");
    // Staff airSteward = new Staff("Air Steward");
    // Staff airSteward2 = new Staff("Air Steward2");

    Staff[] position = { null, null, null, null };

    Passenger[][] seats = {
            { null, null, null },
            { null, null, null },
            { null, null, null },
            { null, null, null },
            { null, null, null },
            { null, null, null },
            { null, null, null }
    };

    Bag[][] hull = {
            { null, null, null },
            { null, null, null },
            { null, null, null },
            { null, null, null },
            { null, null, null },
            { null, null, null },
            { null, null, null }

    };
    private String planeName;

    public Plane(String planeName) {
        this.planeName = planeName;
    }

    public String getPlane() {
        return this.planeName;
    }

    public Boolean isSeatEmpty(BoardingPass pass) {
        int row = pass.getSeat()[0];
        int seat = pass.getSeat()[1];
        return this.seats[row][seat] == null;

    }

    public void boardPassenger(Passenger passenger) {
        int row = passenger.getBoardingPass().getSeat()[0];
        int seat = passenger.getBoardingPass().getSeat()[1];
        this.seats[row][seat] = passenger;
    }

    public Staff assignStaff(Staff staff) {
        int job = staff.getPosition()[0];
        return this.position[job] = staff;
    }

    public Boolean isHullEmpty(BoardingPass pass) {
        int row = pass.getSeat()[0];
        int seat = pass.getSeat()[1];
        return this.hull[row][seat] == null;

    }

    public void PutBagInHull(Bag bag) {
        int row = bag.getBoardingPass().getSeat()[0];
        int seat = bag.getBoardingPass().getSeat()[1];
        this.hull[row][seat] = bag;
    }

}
